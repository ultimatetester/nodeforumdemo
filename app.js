var express = require('express');
var ejs = require('ejs');
var nodeforum = require('nodeforum');
var app = express();

nodeforum.initialize(app, '/forum');
app.set('view engine', 'ejs');

app.get('/', function (req, res) {
    res.send('Navigate to /forum to enter the forum.');
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});